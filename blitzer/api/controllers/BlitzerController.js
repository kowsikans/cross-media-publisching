module.exports = {
  getSpeedCams: function (req, res) {
    var mysql = require("mysql");

    var socket = req.socket;
    var io = sails.io;


    var con = mysql.createConnection({
        host: "kcloud",
        port:3306,
        user: "root",
        password: "root",
        database:"blitzer"
    });

    con.connect(function(err){
        if(err){
            console.log('Error connecting to Db');
            return;
        }
        console.log('Connection established');
    });

    var query ="SELECT st_X(blitz_point) AS lon, st_Y(blitz_point) AS lat FROM blitz WHERE MBRContains(ST_GeomFromText( 'POLYGON((10.016785 48.865103, 10.008545 48.801825, 10.203209 48.795267, 10.200119 48.873233, 10.016785 48.865103))'), blitz_point );";
    
    var myList = new Array();
    
    con.query(query, function(err, rows, fields) {
        if (err) throw err;
        var response = "{ \"speedcams\": [ ";
        for (var i = 0; i < rows.length; i++) {
           //  if(i>0)
           //     response = response + ',';
            
          //  response = response +'{ "lat" : "'+ rows[i].lat +'", "lon" : "'+ rows[i].lon+'"}';
           
           var newObj = new Object();
           newObj.lat=rows[i].lat ;
           newObj.lon=rows[i].lon;
           myList.push(newObj);
           
        }
        io.sockets.in('speedcams').emit('positions', {thisIs: 'theMessage'});

        res.send(JSON.stringify(myList));
      //  response=response +"]}";
    });

    con.end();
     return res;
  },
  
  
  bye: function (req, res) {
    return res.redirect("http://www.sayonara.com");
  }
};